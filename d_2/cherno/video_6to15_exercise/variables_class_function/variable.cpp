#include <iostream>

int main(){
    int anInt;
    char aChar;
    float aFloat;
    std::cout<<"int and long int has a size : "<<sizeof(anInt)<< " " << sizeof(long int)<<std::endl;
    std::cout<<"char has size : "<<sizeof(aChar)<<std::endl;
    std::cout<<"float has a size : "<<sizeof(float)<<std::endl;
    std::cout<<"double has size : "<<sizeof(double)<<std::endl;
    std::cout<<"u int has size : "<<sizeof(unsigned int)<<std::endl;
}